# Drupal Interview Challenge 1
You have 24 hours from the receipt of this document to:

- Write the code to complete the challenge
- Push it up to a repository in [GitHub][3] or [BitBucket][4]
- Send us the url to the repository.

We expect to clone the repository, examine, and run your working code.


## The Challenge
Create a Drupal 6 custom module that responds to the following
request with a responsive page.

- [http://localhost/cnnbrk-tweets][0]

The cnnbrk-tweets page should list the last ten tweets from the [@cnnbrk][1]
twitter handle.  On a small screen (less than 600px wide), it should list all
ten tweets in a single column.  On a larger screen (greater than 600px wide), it
should list all ten tweets divided into two columns.

    small screen      larger screen
    +----------+    +-------+-------+
    |  tweet   |    | tweet | tweet |
    |  tweet   |    | tweet | tweet |
    |  tweet   |    | tweet | tweet |
    |  tweet   |    +-------+-------+
    |  tweet   |
    |  tweet   |
    +----------+


## Assumptions
- Core Drupal 6 already setup and running on the localhost.  Your code does
  not need to account for installing Drupal 6 core on the systems that do not have it.
  You however, will need to install it to develop this.

- Follow best practices for both Drupal and front-end development.

## Tips
- Do your best to impress us with your knowledge of HTML5 and CSS3

- If you use javascript, passing [jslint][2] will help.


## Extra Credit
- Make the tweets from twitter that have links in them be clickable on your
  page.

- Making the page looks good help too.

- Using new client-side technologies are cool.




[0]: http://localhost/cnnbrk-tweets
[1]: https://twitter.com/cnnbrk
[2]: http://jslint.com
[3]: http://github.com
[4]: http://bitbucket.org
